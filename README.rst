======================
Linux Desktop for Rdio
======================
--------------------------------------
A minimal desktop application for Rdio
--------------------------------------

Features:
 - a window for Rdio that doesn't get lost in your tabs
 - media keys

Not supported:
 - downloads
 - collection matching
 - mini player

Really, I just wanted media keys support.